TEST REIGN

DESCRIPTION
    This is an application for the evaluation the Reign. It was made up with a client component created in Angular, and a backend component written in NestJS connected to a mongodb database server.

DEMO
    http://reign.toapp.com.co/

REQUIREMENTS:
    1. Install Node package manager (npm)
    2. MongoDB server.

INSTALLATION (DEV enviroment)
    1. Clone repository from Gitlab.
    2. Install dependencies the frontend applicattion, access to "front" folder and run "npm install" from cli.
    2. Install dependencies the backend applicattion, access to "api" folder and run "npm install" from cli.
    3. In the project of backend access to file "app.module.ts" and specified ip host of mongodb server.
    4. Runs application, for front run "ng serve" and for backend "npm run start:dev".

INSTALLATION (PROD enviroment)
    1. For production environment generate the docker image with the following command "docker build -t reign-front ." inside the directory "front".
    1. For production environment generate the docker image with the following command "docker build -t reign-back ." inside the directory "api".


