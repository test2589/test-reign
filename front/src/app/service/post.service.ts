import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  url: string = environment.api;

  constructor(
    private http: HttpClient
  ) { }

  getPosts() {
    return this.http.get(this.url + '/post');
  }
}
