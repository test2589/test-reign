export interface IPost {
    name: string;
    date: string;
    parent_id: string;
}
