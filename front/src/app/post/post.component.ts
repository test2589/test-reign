import { Component, OnInit } from '@angular/core';
import { IPost } from '../model/post.model';
import { PostService } from '../service/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  posts: IPost[] = [];

  constructor(
    private postService: PostService
  ) { }

  ngOnInit(): void {
    this.postService.getPosts().subscribe((data: IPost[]) => {
      if (data) {
        this.posts = data;
      }
    });
  }

}
