import { HttpModule, Module } from '@nestjs/common';
import { PostService } from './post.service';
import { PostSchedulerService } from './post-scheduler.service';
import { PostController } from './post.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Post, PostSchema } from './model/post.model';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Post.name, schema: PostSchema }]),
    HttpModule,
  ],
  providers: [PostService, PostSchedulerService],
  controllers: [PostController],
})
export class PostModule {}
