import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Post extends Document {
  @Prop()
  parent_id: string;

  @Prop()
  name: string;

  @Prop()
  date: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
