import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

// dto
import { PostDto } from './dto/post.dto';

// models
import { Post } from './model/post.model';

@Injectable()
export class PostService {
  constructor(
    @InjectModel(Post.name) private readonly postModel: Model<Post>,
  ) {}

  public async GetAll(): Promise<Post[]> {
    return await this.postModel.find().exec();
  }

  public exist(parentId: string): boolean {
    const entity = this.postModel.find({ parent_id: parentId }).exec();
    if (entity) {
      return true;
    }
    return false;
  }

  public async create(dto: PostDto) {
    const newPost = await new this.postModel(dto);
    return newPost.save();
  }
}
