export class PostDto {
  readonly parent_id: string;
  readonly name: string;
  readonly date: string;
}
