import { Body, Controller, Get, HttpStatus, Post, Res } from '@nestjs/common';

// dto
import { PostDto } from './dto/post.dto';

// services
import { PostService } from './post.service';

@Controller('post')
export class PostController {
  constructor(private postService: PostService) {}

  @Get()
  public async getAll(@Res() res) {
    const customers = await this.postService.GetAll();
    return res.status(HttpStatus.OK).json(customers);
  }

  @Post()
  public async create(@Res() res, @Body() dto: PostDto) {
    try {
      const result = await this.postService.create(dto);
      return res.status(HttpStatus.OK).json({
        message: 'Post created',
        result,
      });
    } catch (err) {
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error',
        status: 400,
      });
    }
  }
}
