import { HttpService, Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

// service
import { PostService } from './post.service';

@Injectable()
export class PostSchedulerService {
  constructor(
    private postService: PostService,
    private httpService: HttpService,
  ) {}

  @Cron('*/55 * * * * *')
  public getData() {
    console.log('run scheduler');
    return this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .subscribe((data: any) => {
        if (data) {
          data.data.hits.forEach((m) => {
            if (!this.postService.exist(m.parent_id)) {
              this.postService.create({
                date: m.created_at_i,
                name: m.story_title,
                parent_id: m.parent_id,
              });
            }
          });
        }
      });
  }
}
